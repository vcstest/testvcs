package com.example.kaotheflash.myapplication;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.QuickContactBadge;

public class MainazActivity extends AppCompatActivity {

    private ListView Listviewdata;
    private Button Bntback;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainaz);

        Listviewdata = findViewById(R.id.Listviewaz);
        Bntback = findViewById(R.id.BtnBl);



        int[] intsPic = new int[]{R.drawable.aaa, R.drawable.bbb, R.drawable.ccc, R.drawable.ddd, R.drawable.eee
                , R.drawable.fff, R.drawable.ggg, R.drawable.hhh, R.drawable.iii, R.drawable.jjj
                , R.drawable.kkk, R.drawable.lll, R.drawable.mmm, R.drawable.nnn, R.drawable.ooo
                , R.drawable.ppp, R.drawable.qqq, R.drawable.rrr, R.drawable.sss, R.drawable.ttt
                , R.drawable.uuu, R.drawable.vvv, R.drawable.www, R.drawable.xxx, R.drawable.yyy, R.drawable.zzz};

        String[] stringsName = getResources().getStringArray(R.array.data_az);
        MainAdapter mainAdapter = new MainAdapter(this,intsPic,stringsName);
        Listviewdata.setAdapter(mainAdapter);

        Bntback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainazActivity.this,MainActivity.class);
                startActivity(intent);
                finish();
            }
        });





        Listviewdata.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                switch (position) {
                    case 0:
                        SoundA();
                        break;
                    case 1:
                        SoundB();
                        break;
                    case 2:
                        SoundC();
                        break;
                    case 3:
                        SoundD();
                        break;
                    case 4:
                        SoundE();
                        break;
                    case 5:
                        SoundF();
                        break;
                    case 6:
                        SoundG();
                        break;
                    case 7:
                        SoundH();
                        break;
                    case 8:
                        SoundI();
                        break;
                    case 9:
                        SoundJ();
                        break;
                    case 10:
                        SoundK();
                        break;
                    case 11:
                        SoundL();
                        break;
                    case 12:
                        SoundM();
                        break;
                    case 13:
                        SoundN();
                        break;
                    case 14:
                        SoundO();
                        break;
                    case 15:
                        SoundP();
                        break;
                    case 16:
                        SoundQ();
                        break;
                    case 17:
                        SoundR();
                        break;
                    case 18:
                        SoundS();
                        break;
                    case 19:
                        SoundT();
                        break;
                    case 20:
                        SoundU();
                        break;
                    case 21:
                        SoundV();
                        break;
                    case 22:
                        SoundW();
                        break;
                    case 23:
                        SoundX();
                        break;
                    case 24:
                        SoundY();
                        break;
                    case 25:
                        SoundZ();
                        break;


                }
            }
        });


    }
    private void SoundA(){
        MediaPlayer mediaPlayer = MediaPlayer.create(getBaseContext(),R.raw.a);
        mediaPlayer.start();
    }
    private void SoundB(){
        MediaPlayer mediaPlayer = MediaPlayer.create(getBaseContext(),R.raw.b);
        mediaPlayer.start();
    }
    private void SoundC(){
        MediaPlayer mediaPlayer = MediaPlayer.create(getBaseContext(),R.raw.c);
        mediaPlayer.start();
    }
    private void SoundD(){
        MediaPlayer mediaPlayer = MediaPlayer.create(getBaseContext(),R.raw.d);
        mediaPlayer.start();
    }
    private void SoundE(){
        MediaPlayer mediaPlayer = MediaPlayer.create(getBaseContext(),R.raw.e);
        mediaPlayer.start();
    }
    private void SoundF(){
        MediaPlayer mediaPlayer = MediaPlayer.create(getBaseContext(),R.raw.f);
        mediaPlayer.start();
    }
    private void SoundG(){
        MediaPlayer mediaPlayer = MediaPlayer.create(getBaseContext(),R.raw.g);
        mediaPlayer.start();
    }
    private void SoundH(){
        MediaPlayer mediaPlayer = MediaPlayer.create(getBaseContext(),R.raw.h);
        mediaPlayer.start();
    }
    private void SoundI(){
        MediaPlayer mediaPlayer = MediaPlayer.create(getBaseContext(),R.raw.i);
        mediaPlayer.start();
    }
    private void SoundJ(){
        MediaPlayer mediaPlayer = MediaPlayer.create(getBaseContext(),R.raw.j);
        mediaPlayer.start();
    }
    private void SoundK(){
        MediaPlayer mediaPlayer = MediaPlayer.create(getBaseContext(),R.raw.k);
        mediaPlayer.start();
    }
    private void SoundL(){
        MediaPlayer mediaPlayer = MediaPlayer.create(getBaseContext(),R.raw.l);
        mediaPlayer.start();
    }
    private void SoundM(){
        MediaPlayer mediaPlayer = MediaPlayer.create(getBaseContext(),R.raw.m);
        mediaPlayer.start();
    }
    private void SoundN(){
        MediaPlayer mediaPlayer = MediaPlayer.create(getBaseContext(),R.raw.n);
        mediaPlayer.start();
    }
    private void SoundO(){
        MediaPlayer mediaPlayer = MediaPlayer.create(getBaseContext(),R.raw.o);
        mediaPlayer.start();
    }
    private void SoundP(){
        MediaPlayer mediaPlayer = MediaPlayer.create(getBaseContext(),R.raw.p);
        mediaPlayer.start();
    }
    private void SoundQ(){
        MediaPlayer mediaPlayer = MediaPlayer.create(getBaseContext(),R.raw.q);
        mediaPlayer.start();
    }
    private void SoundR(){
        MediaPlayer mediaPlayer = MediaPlayer.create(getBaseContext(),R.raw.r);
        mediaPlayer.start();
    }
    private void SoundS(){
        MediaPlayer mediaPlayer = MediaPlayer.create(getBaseContext(),R.raw.s);
        mediaPlayer.start();
    }
    private void SoundT(){
        MediaPlayer mediaPlayer = MediaPlayer.create(getBaseContext(),R.raw.t);
        mediaPlayer.start();
    }
    private void SoundU(){
        MediaPlayer mediaPlayer = MediaPlayer.create(getBaseContext(),R.raw.u);
        mediaPlayer.start();
    }
    private void SoundV(){
        MediaPlayer mediaPlayer = MediaPlayer.create(getBaseContext(),R.raw.v);
        mediaPlayer.start();
    }
    private void SoundW(){
        MediaPlayer mediaPlayer = MediaPlayer.create(getBaseContext(),R.raw.w);
        mediaPlayer.start();
    }
    private void SoundX(){
        MediaPlayer mediaPlayer = MediaPlayer.create(getBaseContext(),R.raw.x);
        mediaPlayer.start();
    }
    private void SoundY(){
        MediaPlayer mediaPlayer = MediaPlayer.create(getBaseContext(),R.raw.y);
        mediaPlayer.start();
    }
    private void SoundZ(){
        MediaPlayer mediaPlayer = MediaPlayer.create(getBaseContext(),R.raw.z);
        mediaPlayer.start();
    }
}
