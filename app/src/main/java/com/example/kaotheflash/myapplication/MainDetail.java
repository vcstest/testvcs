package com.example.kaotheflash.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainDetail extends AppCompatActivity {

    private Button BtnBBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_detail);

        BtnBBack = findViewById(R.id.Btnbback);

        BtnBBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainDetail.this,MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
