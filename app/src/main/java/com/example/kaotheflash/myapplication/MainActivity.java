package com.example.kaotheflash.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class  MainActivity extends AppCompatActivity {

    private Button BtnAA;
    private Button BtnBB;
    private  Button Btnexit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BtnAA = findViewById(R.id.BtnA);
        BtnBB = findViewById(R.id.BtnB);
        Btnexit = findViewById(R.id.Btnex);

        BtnAA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,MainazActivity.class);
                startActivity(intent);
                finish();
            }
        });

        BtnBB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,MainDetail.class);
                startActivity(intent);
                finish();
            }
        });

        Btnexit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                System.exit(0);
            }
        });
    }
}
